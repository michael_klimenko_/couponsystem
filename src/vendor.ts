// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
// RxJS
import 'rxjs';
//JQuery
//import 'jquery';
// JWT Security
import "angular2-jwt/angular2-jwt";
// Bootstrap CSS
//import 'angular2-bootstrap';
//Materialize CSS
//import "angular2-materialize";
//import 'materializecss'
// import 'materialize';
//import 'script!./app/utils/helper.js';

