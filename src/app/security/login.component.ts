import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService, User, Result} from "./authentication.service";
import {LoggerService} from "../utils/logger.service";

@Component({
    selector: 'login-form',
    templateUrl: 'login.component.html'
})

export class LoginComponent {



    public user = new User('', '', 'ADMIN');


    clientTypeOptions = [
        'CUSTOMER',
        'COMPANY',
        'ADMIN',
    ];

    response: Result;

    constructor(private _router: Router,
                private _service: AuthenticationService,
                private _logger: LoggerService) {
    }

    login() {
        this._service.login(this.user)
            .then(
                data => {
                    this.response = data;
                    if (!this.response.allowed) {
                        //show error in case of failure
                        this._logger.setLog(this.response.result);
                    } else {
                        //Success
                        this._service.setToken(this.response.result);

                        this._logger.setLog('Success!');

                        //redirect to requested Url if any, else, redirect to dashboard
                        this._router.navigate([this._service.redirectUrl === undefined ? 'dashboard' : this._service.redirectUrl]);
                    }
                })
            .catch(err => {
                this._logger.setLog(err);
            });

    }
}
