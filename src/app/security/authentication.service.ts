import {Injectable, EventEmitter} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {Router} from '@angular/router';
import {JwtHelper} from "angular2-jwt/angular2-jwt";
import {Token} from "../entities/token";



export class User {
    constructor(public username: string,
                public password: string,
                public clientType: string) {
    }
}


export class Result {
    allowed: boolean;
    result: string;
}


@Injectable()
export class AuthenticationService {

    jwtHelper: JwtHelper = new JwtHelper();

    public tokenUpdated: EventEmitter<Token> = new EventEmitter<Token>();

    private tokenObj: Token;
    private tokenExpired = true;

    redirectUrl: string;

    constructor(private _router: Router,
                private _http: Http) {
    }

    logout() {
        this.unsetToken();
        this._router.navigate(['login']);
    }

    gotoLogin() {
        this._router.navigate(['login']);
    }

    login(user: User) {

        let headers = new Headers({'Content-Type': 'application/json'});

        return this._http
            .post(process.env.loginUrl, JSON.stringify(user), {headers: headers})
            .toPromise()
            .then(res => res.json())
            .catch(this.handleError);

    }

    authenticated(): boolean {
        return !this.tokenExpired;
    }

    checkCredentials() {
        console.log('AuthenticationService checkCredentials: ', this.authenticated());

        if (!this.authenticated()) {
            this._router.navigate(['login']);
        }
        //else {
        //    this.useJwtHelper();
        //}
    }

    useJwtHelper() {
        var token = localStorage.getItem('id_token');

        if (token != null && token != '') {

            this.setToken(token);


        }
    }

    isUserInRole(userRole: string): boolean {

        const token: string = localStorage.getItem("id_token");

        const validToken = token && !this.jwtHelper.isTokenExpired(token, null);

        if (validToken) {

            var decoded: any;
            decoded = this.jwtHelper.decodeToken(token);

            if (typeof decoded.role === "undefined") {
                return null;
            } else {
                var role = decoded.role;

                return role === userRole;
            }
        } else {
            return null
        }

    }

    unsetToken() {
        localStorage.removeItem("id_token");
        this.tokenObj = null;
        this.tokenExpired = true;
        this.tokenUpdated.emit(this.tokenObj);
    }

    setToken(token: string) {
        localStorage.setItem("id_token", token);
        this.tokenObj = this.jwtHelper.decodeToken(token);
        this.tokenObj.exp = this.jwtHelper.getTokenExpirationDate(token);
        this.tokenExpired = this.jwtHelper.isTokenExpired(token);
        this.tokenUpdated.emit(this.tokenObj);
    }

    getToken(): Token {
        return this.tokenObj;
    }

    private handleError(error: any) {
        return Promise.reject(error.message || error);
    }
}
