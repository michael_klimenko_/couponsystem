import {Coupon} from "./coupon";
export class Company {
  id: number;
  comp_name: string;
  password: string;
  email:string;
  coupons: Coupon[];
}
