import {Coupon} from "./coupon";

export class Customer {
  id: number;
  cust_name: string;
  password: string;
  coupons: Coupon[];
}
