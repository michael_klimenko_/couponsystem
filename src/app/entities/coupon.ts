export class Coupon {
  id: number;
  title: string;
  startDate: string;
  endDate: string;
  amount: number;
  type: number;
  message: string;
  price: number;
  image: string
}
