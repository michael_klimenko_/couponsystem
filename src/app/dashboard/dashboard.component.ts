import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CouponService} from "../coupons/coupon.service";
import {Coupon} from "../entities/coupon";
import {AuthenticationService} from "../security/authentication.service";
import {LoggerService} from "../utils/logger.service";
import {CustomerService} from "../customers/customer.service";
import {CompanyService} from "../companies/company.service";

@Component({
    selector: 'my-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    //Used for customer
    purchasedCoupons: Coupon[] = [];
    coupons: Coupon[] = [];

    //Used for company
    companyCoupons: Coupon[] = [];

    constructor(private _router: Router,
                private _service: AuthenticationService,
                private _couponService: CouponService,
                private _customerService: CustomerService,
                private _companyService: CompanyService,
                private _logger: LoggerService) {
    }

    ngOnInit() {

        console.log('dashboard loaded for ' + this._service.getToken().role);


        //CUSTOMER Login
        if (this._service.getToken().role === "CUSTOMER") {

            //load all coupons
            this._couponService.getCoupons()
                .then(
                    coupons => this.coupons = coupons//.slice(1, 5)
                ).catch(err => {
                this._logger.setLog(err);
            });

            //load all purchased coupons
            this._customerService.getAllCustomerCoupons(this._service.getToken().myId)
                .then(
                    coupons => this.purchasedCoupons = coupons
                ).catch(err => {
                this._logger.setLog(err);
            });
        }

    }

    gotoDetail(coupon: Coupon) {
        this._router.navigate(['coupon/detail', coupon.id]);
    }

    purchase(coupon: Coupon) {
        this._couponService.purchase(coupon)
            .then(coupon => {
                this.purchasedCoupons.push(coupon);
                this._logger.setLog("Purchased coupons: " + JSON.stringify(coupon));
            })
            .catch(err => {
                this._logger.setLog(err);
            });
    }


}
