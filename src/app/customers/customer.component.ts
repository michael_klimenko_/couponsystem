import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Customer} from "../entities/customer";
import {CustomerService} from "./customer.service";
import {LoggerService} from "../utils/logger.service";
import {Coupon} from "../entities/coupon";


@Component({
    selector: 'customers',
    templateUrl: 'customer.component.html',
})

export class CustomerComponent implements OnInit {
    title = 'Customers';
    customers: Customer[];
    filteredCustomers: Customer[];
    selectedCustomer: Customer;
    customerFilter:string;

    constructor(
                private _router: Router,
                private _customerService: CustomerService,
                private _logger: LoggerService
    ) {

    }

    getCustomers() {
        this._customerService.getCustomers()
            .then((response) => {
                this.customers = response;
                this.onCustomerFilterChange()
            })
            .catch(err => {
                this._logger.setLog(err);
            });
    }

    ngOnInit() {
        console.log('CustomerComponent  ngOnInit');

        this.getCustomers();
    }

    onSelect(customer: Customer) {
        this.selectedCustomer = customer;
    }

    gotoDetail() {
        this._router.navigate(['customer/detail', this.selectedCustomer.id]);
    }

    gotoCreate() {
        this._router.navigate(['customer/create']);
    }

    gotoCoupon(coupon: Coupon) {
        this._router.navigate(['coupon/detail', coupon.id]);
    }


    gotoDelete() {
        this._router.navigate(['customer/delete', this.selectedCustomer.id]);
    }

    onCustomerFilterChange() {
        this.filteredCustomers = this.filterCustomers(this.customerFilter);
    }

    filterCustomers(filter:string):Customer[] {
        return (filter && filter !== '') ? this.customers.filter( (customer) => customer.cust_name.toLocaleLowerCase().indexOf(filter.toLocaleLowerCase()) !== -1) : this.customers
    }

}


