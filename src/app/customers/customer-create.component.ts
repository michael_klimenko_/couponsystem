import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Customer} from "../entities/customer";
import {CustomerService} from "./customer.service";
import {LoggerService} from "../utils/logger.service";
import {UserService} from "../utils/forms/fields/user.service";

@Component({
    selector: 'my-customer-create',
    templateUrl: 'customer-create.component.html',
    providers:  [UserService]
})
export class CustomerCreateComponent implements OnInit {

    customer: Customer;
    fields:any[];

    constructor(private _router: Router,
                private _customerService: CustomerService,
                private _userService: UserService,
                private _logger: LoggerService) {
    }

    ngOnInit() {
        this.clear()
        this.fields = this._userService.getFields("Customer-create",{});
    }

    save(customer: Customer) {

        this._customerService.save(customer)
            .then(
                customer =>  {
                    this.customer = customer;
                    this._logger.setLog('Created: ' + JSON.stringify(this.customer));
                    this._router.navigate(['customers']);
                }
            ).catch(err => {
            this._logger.setLog(err);
        });


    }

    clear() {
        this.customer = new Customer();
    }

    goBack() {
        window.history.back();
    }
}
