import { Component,  OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Customer} from "../entities/customer";
import {CustomerService} from "./customer.service";
import {LoggerService} from "../utils/logger.service";
import {Subscription} from "rxjs";
import {UserService} from "../utils/forms/fields/user.service";

@Component({
    selector: 'customer-detail',
    templateUrl: 'customer-detail.component.html',
    providers: [UserService]

})
export class CustomerDetailComponent implements OnInit {

    private sub: Subscription;
    customer:Customer;
    fields: any[];

    constructor(private _router:Router,
                private _route: ActivatedRoute,
                private _customerService:CustomerService,
                private _userService: UserService,
                private _logger:LoggerService) {
    }

    save(customer:Customer) {

        customer.id = this.customer.id;

        this._customerService.save(customer)
            .then(
                customer => {
                    this.customer = customer
                    this._logger.setLog('Updated: ' + JSON.stringify(this.customer));
                    this._router.navigate(['customers']);
                }
            ).catch(err => {
            this._logger.setLog(err);
        });


    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._customerService.getCustomer(id).then( (customer) => {
                this.customer = customer
                this.fields = this._userService.getFields("Customer-update", this.customer);
                this._logger.setLog(" this.fields = " + JSON.stringify(this.fields));
            });
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }

}
