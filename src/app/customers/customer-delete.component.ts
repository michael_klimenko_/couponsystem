import {Component, OnInit} from '@angular/core';

import {Customer} from "../entities/customer";
import {CustomerService} from "./customer.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {LoggerService} from "../utils/logger.service";

@Component({
    selector: 'customer-delete',
    templateUrl: 'customer-delete.component.html'

})
export class CustomerDeleteComponent implements OnInit {

    private sub: Subscription;
    customer: Customer;

    constructor(private _router: Router,
                private _customerService: CustomerService,
                private _route: ActivatedRoute,
                private _logger: LoggerService) {
    }

    delete(customer: Customer) {

        this._customerService.delete(customer)
            .then(
                () => {
                    this._logger.setLog('Deleted: ' + JSON.stringify(this.customer));
                    this._router.navigate(['customers']);
                }
            )
            .catch(err => {
                this._logger.setLog(err);
            });


    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._customerService.getCustomer(id).then(company => this.customer = company);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }
}
