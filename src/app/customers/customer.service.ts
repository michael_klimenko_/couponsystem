import {Injectable}    from '@angular/core';
import {AuthHttp} from "angular2-jwt/angular2-jwt";
import {Customer} from "../entities/customer";
import {Coupon} from "../entities/coupon";
import 'rxjs/add/operator/toPromise';


@Injectable()
export class CustomerService {


    constructor(private _authHttp: AuthHttp) {
    }

    getCustomers(): Promise<Customer[]> {
        return this._authHttp.get(process.env.customersUrl)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    getCustomer(id: number) {
        return this.getCustomers()
            .then(customers => customers.filter(customer => customer.id === id)[0]);
    }

    getAllCustomerCoupons(customerId: Number): Promise<Coupon[]> {

        let url = `${process.env.customersUrl}/${customerId}/coupons`;

        return this._authHttp.get(url)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    save(customer: Customer): Promise<Customer> {
        if (customer.id) {
            return this.put(customer);
        }
        return this.post(customer);
    }

    delete(customer: Customer) {

        let url = `${process.env.customersUrl}/${customer.id}`;

        return this._authHttp
            .delete(url)
            .toPromise()
            .catch(this.handleError);
    }


    // Add new Customer
    private post(customer: Customer): Promise<Customer> {

        return this._authHttp
            .post(process.env.customersUrl, JSON.stringify(customer))
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    // Update existing Customer
    private put(customer: Customer) {

        let url = `${process.env.customersUrl}/${customer.id}`;

        return this._authHttp
            .put(url, JSON.stringify(customer))
            .toPromise()
            .then(() => customer)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.message || error);
    }
}
