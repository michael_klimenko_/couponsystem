import {NgModule} from '@angular/core';
import {BrowserModule}  from '@angular/platform-browser';
import {ReactiveFormsModule, FormsModule}    from '@angular/forms';
import {HttpModule, Http} from "@angular/http";
import {AuthHttp, AuthConfig} from "angular2-jwt/angular2-jwt";
import { MaterialModule} from "@angular/material"

import {AppComponent} from './app.component';
import {AppRoutingModule, routingComponents} from "./app.routes";

//Customer component
import {CustomerService} from "./customers/customer.service";

//Company components
import {CompanyService} from "./companies/company.service";

//Coupon components
import {CouponService} from "./coupons/coupon.service";

import {LoggerService} from "./utils/logger.service";
import {AuthenticationService} from "./security/authentication.service";

import {AuthGuard} from "./security/auth-guard.service";

import {LocationStrategy, HashLocationStrategy} from "@angular/common";

//Dynamic Form components
import {DynamicFormComponent} from "./utils/forms/fields/dynamic-form.component";
import {DynamicFormFieldComponent} from "./utils/forms/fields/dynamic-form-field.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        AppRoutingModule,
        MaterialModule.forRoot()
    ],
    declarations: [
        AppComponent,
        routingComponents,
        DynamicFormComponent,
        DynamicFormFieldComponent
    ],
    providers: [
        LoggerService,
        AuthenticationService,
        AuthGuard,
        CustomerService,
        CouponService,
        CompanyService,
        {provide: LocationStrategy, useClass: HashLocationStrategy },
        {provide: AuthHttp,
            useFactory: (http:Http) => {
                return new AuthHttp(new AuthConfig({
                    globalHeaders: [
                        {'Content-Type':'application/json'}
                    ],
                    noJwtError: true,
                }), http);
            },
            deps: [Http]
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
