import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Company} from "../entities/company";
import {CompanyService} from "./company.service";
import {LoggerService} from "../utils/logger.service";
import {UserService} from "../utils/forms/fields/user.service";
//import {QuestionService} from "../utils/forms/questions/question.service";

@Component({
    selector: 'my-company-create',
    templateUrl: 'company-create.component.html',
    providers:  [UserService]
})
export class CompanyCreateComponent implements OnInit {

    fields:any[];
    company: Company;

    constructor(private _router: Router,
                private _companyService: CompanyService,
                private _userService: UserService,
                private _logger: LoggerService) {
    }

    ngOnInit() {
        this.clear()
        this.fields = this._userService.getFields("Company-create",{});
    }

    save(company: Company) {
        this._companyService.save(company)
            .then(
                company => {
                    this.company = company;
                    this._logger.setLog('Created: ' + JSON.stringify(this.company));
                    this._router.navigate(['companies']);
                }
            ).catch(err => {
            this._logger.setLog(err);
        });
    }

    clear() {
        this.company = new Company();
    }

    goBack() {
        window.history.back();
    }
}
