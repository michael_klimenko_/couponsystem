import { Injectable }    from '@angular/core';
import {AuthHttp} from "angular2-jwt/angular2-jwt";
import {Company} from "../entities/company";
import {Coupon} from "../entities/coupon";
import 'rxjs/add/operator/toPromise';


@Injectable()
export class CompanyService {

    constructor(
                private _authHttp:AuthHttp) {
    }

    getCompanies():Promise<Company[]> {

        return this._authHttp.get(process.env.companiesUrl)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    getCompany(id:number) {
        return this.getCompanies()
            .then(companies => companies.filter(company => company.id === id)[0]);
    }

    getAllCompanyCoupons(companyId:number):Promise<Coupon[]>{

        let url = `${process.env.companiesUrl}/${companyId}/coupons`;

        return this._authHttp.get(url)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    save(company:Company):Promise<Company> {
        if (company.id) {
            return this.put(company);
        }
        return this.post(company);
    }

    delete(company:Company) {

        let url = `${process.env.companiesUrl}/${company.id}`;

        return this._authHttp
            .delete(url)
            .toPromise()
            .catch(this.handleError);
    }

    // Add new Company
    private post(company:Company):Promise<Company> {

        return this._authHttp
            .post(process.env.companiesUrl, JSON.stringify(company))
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    // Update existing Company
    private put(company:Company) {

        let url = `${process.env.companiesUrl}/${company.id}`;

        return this._authHttp
            .put(url, JSON.stringify(company))
            .toPromise()
            .then(() => company)
            .catch(this.handleError);
    }

    private handleError(error:any) {
        return Promise.reject(error.message || error);
    }
}
