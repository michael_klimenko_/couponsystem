import {Component, OnInit} from '@angular/core';

import {Company} from "../entities/company";
import {CompanyService} from "./company.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {LoggerService} from "../utils/logger.service";

@Component({
    selector: 'company-delete',
    templateUrl: 'company-delete.component.html'

})
export class CompanyDeleteComponent implements OnInit {

    private sub: Subscription;
    company: Company;

    constructor(private _router: Router,
                private _companyService: CompanyService,
                private _route: ActivatedRoute,
                private _logger: LoggerService) {
    }

    delete(company: Company) {

        this._companyService.delete(company)
            .then(
                () => {
                    this._logger.setLog('Deleted: ' + JSON.stringify(this.company));
                    this._router.navigate(['companies']);
                }
            )
            .catch(err => {
                this._logger.setLog(err);
            });


    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._companyService.getCompany(id).then(company => this.company = company);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }
}
