import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Company} from "../entities/company";
import {CompanyService} from "./company.service";
import {LoggerService} from "../utils/logger.service";
import {Coupon} from "../entities/coupon";

@Component({
    selector: 'companies',
    templateUrl: 'company.component.html',
})

export class CompanyComponent implements OnInit {
    title = 'Companies';
    companies: Company[];
    filteredCompanies: Company[];
    selectedCompany: Company;
    companyFilter: string;

    constructor(private _router: Router,
                private _companyService: CompanyService,
                private _logger: LoggerService
    ) {
    }

    getCompanies() {
        this._companyService.getCompanies()
            .then((response) => {
                this.companies = response;
                this.onCompanyFilterChange();
            })
            .catch(err => {
                this._logger.setLog(err);
            });
    }

    ngOnInit() {
        this.getCompanies();
    }

    createCoupon() {
        this._router.navigate(['coupon/create']);
    }

    onSelect(company: Company) {
        this.selectedCompany = company;
    }

    gotoCoupon(coupon: Coupon) {
        this._router.navigate(['coupon/detail', coupon.id]);
    }

    gotoCreate() {
        this._router.navigate(['company/create']);
    }

    gotoDetail() {
        this._router.navigate(['company/detail', this.selectedCompany.id]);
    }

    gotoDelete() {
        this._router.navigate(['company/delete', this.selectedCompany.id]);
    }

    onCompanyFilterChange() {
        this.filteredCompanies = this.filterCompanies(this.companyFilter);
    }

    filterCompanies(filter:string):Company[] {
        return (filter && filter !== '') ? this.companies.filter( (company) => company.comp_name.toLocaleLowerCase().indexOf(filter.toLocaleLowerCase()) !== -1) : this.companies
    }
}


