import {Component, OnInit} from "@angular/core";
import {Company} from "../entities/company";
import {CompanyService} from "./company.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {LoggerService} from "../utils/logger.service";
import {UserService} from "../utils/forms/fields/user.service";


@Component({
    selector: 'company-detail',
    templateUrl: 'company-detail.component.html',
    providers: [UserService]

})
export class CompanyDetailComponent implements OnInit {

    private sub: Subscription;
    company: Company;
    fields: any[];

    constructor(private _router: Router,
                private _companyService: CompanyService,
                private _route: ActivatedRoute,
                private _userService: UserService,
                private _logger: LoggerService) {
    }

    save(company: Company) {

        company.id = this.company.id

        this._companyService.save(company)
            .then(
                company => {
                    this.company = company
                    this._logger.setLog('Updated: ' + JSON.stringify(this.company))
                    this._router.navigate(['companies'])
                }
            ).catch(err => {
            this._logger.setLog(err);
        });
    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._companyService.getCompany(id).then(
                (company) => {
                    this.company = company
                    this.fields = this._userService.getFields("Company-update", this.company);
                    this._logger.setLog(" this.fields = " + JSON.stringify(this.fields));
                });
        });

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}
