import {Component, OnInit, ViewContainerRef} from "@angular/core";
import {Token} from "./entities/token";
import {AuthenticationService} from "./security/authentication.service";
import {LoggerService} from "./utils/logger.service";
import {MdSnackBar, MdSnackBarConfig} from "@angular/material";
// import as RouterEvent to avoid confusion with the DOM Event
import {Event as RouterEvent, Router, NavigationStart, NavigationCancel, NavigationError, NavigationEnd} from '@angular/router'

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

    isDarkTheme: boolean = false;
    menuItems = [
        {text: 'Dashboard', link: 'dashboard'},
        {text: 'Settings', link: 'settings', disabled: true},
        {text: 'Help', link: 'help', disabled: true},
        {text: 'Sign Out', link: 'logout'}
    ];

    // Sets initial value to true to show loading spinner on first load
    loading: boolean = true;
    private tokenObj: Token;

    constructor(private _router: Router,
                private _service: AuthenticationService,
                private _logger: LoggerService,
                private _snackBar: MdSnackBar,
                private _viewContainerRef: ViewContainerRef) {

        _router.events.subscribe((event: RouterEvent) => {
            this.navigationInterceptor(event);
        });
    }

    ngOnInit() {
        // If user is not authenticated, is redirected to the login page
        this._service.checkCredentials();

        // listen to the user object,
        // once authenticated ,  token object will hold details about logged in user
        this._service.tokenUpdated.subscribe(
            (tokenObj: Token) => {
                this.tokenObj = this._service.getToken();
            }
        );

        //once there is a log , show it
        this._logger.logEvent.subscribe(
            (logMsg: string) => {
                this.showLog(logMsg,5000);
            }
        );
    }

    ngOnDestroy() {
        // prevent memory leak when component is destroyed
        this._service.tokenUpdated.unsubscribe();
        this._logger.logEvent.unsubscribe();
    }

    logout() {
        this._service.logout();
    }

    goBack() {
        window.history.back();
    }


    public showLog(message: string, timeout:number) {

        let actionButtonLabel: string = 'Dismiss';
        let action: boolean = true;
        let config = new MdSnackBarConfig(this._viewContainerRef);
        let _snackBarRef = this._snackBar.open(message, action && actionButtonLabel, config);
        setTimeout(_snackBarRef.dismiss.bind(_snackBarRef), timeout);

    }


    // Shows and hides the loading spinner during RouterEvent changes
    navigationInterceptor(event: RouterEvent): void {
        if (event instanceof NavigationStart) {
            this.loading = true;
        }
        if (event instanceof NavigationEnd) {
            this.loading = false;
        }

        // Set loading state to false in both of the below events to hide the spinner in case a request fails
        if (event instanceof NavigationCancel) {
            this.loading = false;
        }
        if (event instanceof NavigationError) {
            this.loading = false;
        }
    }
}
