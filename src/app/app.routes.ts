//import components used in routing
import {NgModule,ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from '@angular/router';
//Commonly used components
import {DashboardComponent} from "./dashboard/dashboard.component";
import {LoginComponent} from "./security/login.component";

//Customer component
import {CustomerComponent} from "./customers/customer.component";
import {CustomerCreateComponent} from "./customers/customer-create.component";
import {CustomerDetailComponent} from "./customers/customer-detail.component";
import {CustomerDeleteComponent} from "./customers/customer-delete.component";

//Company components
import {CompanyComponent} from "./companies/company.component";
import {CompanyCreateComponent} from "./companies/company-create.component";
import {CompanyDetailComponent} from "./companies/company-detail.component";
import {CompanyDeleteComponent} from "./companies/company-delete.component";

//Coupon components
import {CouponComponent} from "./coupons/coupon.component";
import {CouponCreateComponent} from "./coupons/coupon-create.component";
import {CouponDetailComponent} from "./coupons/coupon-detail.component";
import {CouponDeleteComponent} from "./coupons/coupon-delete.component";

//Security
import {AuthGuard} from "./security/auth-guard.service";

const appRoutes: Routes = <Routes>[

    //
    {path: 'dashboard',             component: DashboardComponent,      canActivate: [AuthGuard], data: { title: 'Dashboard'}},
    //Customer resource routes
    {path: 'customer/detail/:id',   component: CustomerDetailComponent, canActivate: [AuthGuard]},
    {path: 'customer/delete/:id',   component: CustomerDeleteComponent, canActivate: [AuthGuard]},
    {path: 'customer/create',       component: CustomerCreateComponent, canActivate: [AuthGuard]},
    {path: 'customers',             component: CustomerComponent,       canActivate: [AuthGuard]},
    //Company resource routes
    {path: 'company/detail/:id',    component: CompanyDetailComponent,  canActivate: [AuthGuard]},
    {path: 'company/delete/:id',    component: CompanyDeleteComponent,  canActivate: [AuthGuard]},
    {path: 'company/create',        component: CompanyCreateComponent,  canActivate: [AuthGuard]},
    {path: 'companies',             component: CompanyComponent,        canActivate: [AuthGuard]},
    //Coupon resource routes
    {path: 'coupon/detail/:id',     component: CouponDetailComponent,   canActivate: [AuthGuard]},
    {path: 'coupon/delete/:id',     component: CouponDeleteComponent,   canActivate: [AuthGuard]},
    {path: 'coupon/create',         component: CouponCreateComponent,   canActivate: [AuthGuard]},
    {path: 'coupons',               component: CouponComponent,         canActivate: [AuthGuard]},

    //default
    {path: 'login',                 component: LoginComponent},
    {path: '',                      component: LoginComponent},
    { path: '**',                   component: LoginComponent}
];

//export const appRoutingProviders: any[] = [];

//export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
//rc5
//export const routing = RouterModule.forRoot(appRoutes);

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
})
export class AppRoutingModule { }

export const routingComponents = [
    DashboardComponent,
    LoginComponent,
    //
    CustomerComponent,
    CustomerDetailComponent,
    CustomerCreateComponent,
    CustomerDeleteComponent,
    //
    CompanyComponent,
    CompanyDetailComponent,
    CompanyDeleteComponent,
    CompanyCreateComponent,
    //
    CouponComponent,
    CouponDetailComponent,
    CouponDeleteComponent,
    CouponCreateComponent
];

