import {Component, OnInit} from '@angular/core';

import {Coupon} from "../entities/coupon";
import {CouponService} from "./coupon.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {LoggerService} from "../utils/logger.service";


@Component({
    selector: 'coupon-delete',
    templateUrl: 'coupon-delete.component.html'

})
export class CouponDeleteComponent implements OnInit {

    private sub: Subscription;
    coupon: Coupon;

    constructor(private _router: Router,
                private _couponService: CouponService,
                private _route: ActivatedRoute,
                private _logger: LoggerService) {
    }

    delete(coupon: Coupon) {

        this._couponService.delete(coupon)
            .then(
                () => {
                    this._logger.setLog('Deleted: ' + JSON.stringify(this.coupon));
                    this._router.navigate(['coupons']);
                }
            )
            .catch(err => {
                this._logger.setLog(err);
        });


    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._couponService.getCoupon(id).then(coupon => this.coupon = coupon);
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    goBack() {
        window.history.back();
    }
}
