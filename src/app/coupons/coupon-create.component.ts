import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Coupon} from "../entities/coupon";
import {CouponService} from "./coupon.service";
import {LoggerService} from "../utils/logger.service";
import {UserService} from "../utils/forms/fields/user.service";

@Component({
    selector: 'my-coupon-create',
    templateUrl: 'coupon-create.component.html',
    providers: [UserService]
})

export class CouponCreateComponent implements OnInit {

    coupon: Coupon;
    fields: any[];

    constructor(private _router: Router,
                private _couponService: CouponService,
                private _userService: UserService,
                private _logger: LoggerService) {
    }

    ngOnInit() {
        this.clear()
        this.fields = this._userService.getFields("Coupon-create",{});
    }

    save(coupon: Coupon) {

        this._couponService.save(coupon)
            .then(
                coupon => {
                    this.coupon = coupon;
                    this._logger.setLog('Created: ' + JSON.stringify(this.coupon));
                    this._router.navigate(['coupons']);
                }
            ).catch(err => {
            this._logger.setLog(err);
        });
    }

    clear() {
        this.coupon = new Coupon();
    }

    goBack() {
        window.history.back();
    }
}
