import {Component, OnInit, EventEmitter} from "@angular/core";
import {Router} from "@angular/router";
import {Coupon} from "../entities/coupon";
import {CouponService} from "./coupon.service";
import {LoggerService} from "../utils/logger.service";
import {Subscription} from "rxjs";

@Component({
    selector: 'coupons',
    templateUrl: 'coupon.component.html'
})

export class CouponComponent implements OnInit {

    title = 'Coupons';
    coupons: Coupon[];
    filteredCoupons: Coupon[];
    selectedCoupon: Coupon;
    couponFilter: string = '';

    constructor(private _router: Router,
                private _couponService: CouponService,
                private _logger: LoggerService) {

    }

    ngOnInit() {
        this.getCoupons();
    }

    getCoupons() {
        this._couponService.getCoupons()
            .then( (response) => {
                this.coupons = response;
                this.onCouponFilterChange();
            })
            .catch(err => {
                this._logger.setLog(err);
            });
    }

    onSelect(coupon: Coupon) {
        this.selectedCoupon = coupon;
    }

    gotoDetail() {
        this._router.navigate(['coupon/detail', this.selectedCoupon.id]);
    }

    gotoCreate() {
        this._router.navigate(['coupon/create']);
    }

    gotoDelete() {
        this._router.navigate(['coupon/delete', this.selectedCoupon.id]);
    }

    onCouponFilterChange() {
        this.filteredCoupons = this.filterCoupons(this.couponFilter);
    }

    filterCoupons(filter:string):Coupon[] {
       return (filter && filter !== '') ? this.coupons.filter( (coupon) => coupon.title.toLocaleLowerCase().indexOf(filter.toLocaleLowerCase()) !== -1) : this.coupons
    }

}


