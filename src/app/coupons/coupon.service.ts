import {Injectable}    from '@angular/core';
import {AuthHttp} from "angular2-jwt/angular2-jwt";
import {Coupon} from "../entities/coupon";
import 'rxjs/add/operator/toPromise'


@Injectable()
export class CouponService {

    constructor(
                private _authHttp: AuthHttp) {
    }

    getCoupons(): Promise<Coupon[]> {

        return this._authHttp.get(process.env.couponsUrl)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    getCoupon(id: number) {
        return this.getCoupons()
            .then(coupons => coupons.filter(coupon => coupon.id === id)[0]);
    }

    save(coupon: Coupon): Promise<Coupon> {
        if (coupon.id) {
            return this.put(coupon);
        }
        return this.post(coupon);
    }

    delete(coupon: Coupon) {

        let url = `${process.env.couponsUrl}/${coupon.id}`;

        return this._authHttp
            .delete(url)
            .toPromise()
            .catch(this.handleError);
    }

    // Purchase coupon
    purchase(coupon: Coupon) {

        let url = `${process.env.couponsUrl}/${coupon.id}/purchase`;

        return this._authHttp
            .get(url)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError);
    }

    // Add new Coupon
    private post(coupon: Coupon): Promise<Coupon> {


        return this._authHttp
            .post(process.env.couponsUrl, JSON.stringify(coupon))
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    // Update existing Coupon
    private put(coupon: Coupon) {

        let url = `${process.env.couponsUrl}/${coupon.id}`;

        return this._authHttp
            .put(url, JSON.stringify(coupon))
            .toPromise()
            .then(() => coupon)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        return Promise.reject(error.message || error);
    }
}
