import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Coupon} from "../entities/coupon";
import {CouponService} from "./coupon.service";
import {Subscription} from "rxjs";
import {LoggerService} from "../utils/logger.service";
import {UserService} from "../utils/forms/fields/user.service";
import {User} from "../security/authentication.service";

@Component({
    selector: 'my-coupon-detail',
    templateUrl: 'coupon-detail.component.html',
    providers: [UserService]
})
export class CouponDetailComponent implements OnInit {

    private sub: Subscription;
    coupon: Coupon;
    fields: any[];

    constructor(private _router: Router,
                private _couponService: CouponService,
                private _userService: UserService,
                private _route: ActivatedRoute,
                private _logger: LoggerService) {
    }

    save(coupon: Coupon) {

        coupon.id = this.coupon.id

        this._couponService.save(coupon)
            .then(
                coupon => {
                    this.coupon = coupon
                    this._logger.setLog('Updated: ' + JSON.stringify(this.coupon));
                    this._router.navigate(['coupons']);
                }
            ).catch(err => {
            this._logger.setLog(err);
        });


    }

    ngOnInit() {
        this.sub = this._route.params.subscribe(params => {
            let id = +params['id']; // (+) converts string 'id' to a number
            this._couponService.getCoupon(id).then( (coupon) =>  {
                this.coupon = coupon
                this.fields = this._userService.getFields("Coupon-update",this.coupon)
            });
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    // ngOnInit() {
    //     let id = +this.routeParams.get('id');
    //     this.couponService.getCoupon(id)
    //         .then(coupon => this.coupon = coupon);
    // }

    goBack() {
        window.history.back();
    }
}
