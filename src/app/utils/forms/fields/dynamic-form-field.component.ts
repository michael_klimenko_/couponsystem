import {Component, Input} from "@angular/core";
import {FormBase} from "./form-base";
import {FormGroup} from "@angular/forms";
@Component({
    selector: 'df-field',
    template: `
    <div [formGroup]="form" class="form-group">
    
      <label [attr.for]="field.key" class="control-label">{{field.label}}</label>
      
      <div [ngSwitch]="field.controlType">
      
        <!--<input *ngSwitchCase="'textbox'" [formControlName]="field.key" [id]="field.key" [type]="field.type" class="form-control" [placeholder]="field.placeholder">-->
        
        <md-input *ngSwitchCase="'textbox'"
            [type]="field.type" 
            [id]="field.key"
            [formControlName]="field.key"
            [placeholder]="field.placeholder"
            [value]="field.key" 
            [step]="field.step">
        </md-input>
      
        <select [id]="field.key" *ngSwitchCase="'dropdown'" [formControlName]="field.key" class="form-control">
          <option style="display:none" value="">Choose an option</option>
          <option *ngFor="let opt of field.options" [value]="opt.key">{{opt.value}}</option>
        </select>
        
      </div>
      <div class="errorMessage" *ngIf="!isValid">({{field.placeholder}} is required)</div>
    </div>
`
})
//[disabled]="field.disabled" [readonly]="field.readonly"
export class DynamicFormFieldComponent {
    @Input() field: FormBase<any>;
    @Input() form: FormGroup;

    get isValid() {
        return this.form.controls[this.field.key].valid;
    }
}
