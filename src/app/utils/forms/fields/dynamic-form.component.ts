import {Component, Input, Output, EventEmitter} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {FormBase} from "./form-base";
import {FormControlService} from "./form-control.service";


@Component({
    selector: 'dynamic-form',
    template: `
<div>
  <form (ngSubmit)="onSubmit()" [formGroup]="form">
  
    <div *ngFor="let field of fields" class="form-row">
    
      <df-field [field]="field" [form]="form"></df-field>
      
    </div>
    
    <div class="form-row">
    
      <button md-raised-button type="submit" [disabled]="!form.valid" class="btn btn-success btn-md">Save</button>
      
    </div>
    
  </form>

  <div *ngIf="payLoad" class="form-row">
  
    <br><strong>Saved the following values</strong><br>{{payLoad}}
    
  </div>
  
</div>`,

    providers: [FormControlService]
})

export class DynamicFormComponent {
    @Input() fields: FormBase<any>[] = [];
    @Output('send') submitted: EventEmitter<any> = new EventEmitter();
    form: FormGroup;
    payLoad = '';

    constructor(private qcs: FormControlService) {
    }

    ngOnInit() {
        this.form = this.qcs.toControlGroup(this.fields);
    }

    onSubmit() {
        this.payLoad = JSON.stringify(this.form.value);
        this.submitted.emit(this.form.value);
    }
}
