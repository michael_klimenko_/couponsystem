import {Injectable} from "@angular/core";
import {FormBase} from "./form-base";
import {TextboxField} from "./textbox-field";
import {DropdownField} from "./dropdown-field";
import {Customer} from "../../../entities/customer";
import {Company} from "../../../entities/company";
import {Coupon} from "../../../entities/coupon";
import {LoggerService} from "../../logger.service";

@Injectable()
export class UserService {

    constructor(private _logger: LoggerService) {
    }

    getFields(objectType: string, objectModel: any) {

        //this._logger.setLog("got objectModel = " + JSON.stringify(objectModel));

        let fields: FormBase<any>[];

        switch (objectType) {

            case "Customer-create":
                fields = [
                    new TextboxField({
                        key: 'cust_name',
                        required: true,
                        value: '',
                        placeholder: 'Name',
                        order: 1
                    }),
                    new TextboxField({
                        key: 'password',
                        type: 'password',
                        required: true,
                        value: '',
                        placeholder: 'Password',
                        order: 2
                    })
                ];
                break;

            case "Customer-update":
                fields = [
                    new TextboxField({
                        key: 'password',
                        label: 'Password',
                        type: 'password',
                        required: true,
                        value: objectModel.password,
                        order: 1
                    })
                ];
                break;

            case "Company-create":
                fields = [
                    new TextboxField({
                        key: 'comp_name',
                        required: true,
                        value: '',
                        placeholder: 'Company Name',
                        order: 1
                    }),
                    new TextboxField({
                        key: 'password',
                        type: 'password',
                        required: true,
                        value: '',
                        placeholder: 'Password',
                        order: 2
                    }),
                    new TextboxField({
                        key: 'email',
                        type: 'email',
                        required: true,
                        value: '',
                        placeholder: 'Email',
                        order: 3
                    })
                ];
                break;

            case "Company-update":
                fields = [
                    new TextboxField({
                        key: 'password',
                        label: 'Password',
                        type: 'password',
                        required: true,
                        value: objectModel.password,
                        order: 1
                    }),
                    new TextboxField({
                        key: 'email',
                        label: 'Email',
                        type: 'email',
                        required: true,
                        value: objectModel.email,
                        order: 2
                    })
                ];
                break;

            case "Coupon-create":
                fields = [
                    new DropdownField({
                        key: 'type',
                        label: 'Category',
                        options: [
                            {key: 0, value: 'RESTAURANTS'},
                            {key: 1, value: 'ELECTRICITY'},
                            {key: 2, value: 'FOOD'},
                            {key: 3, value: 'HEALTH'},
                            {key: 4, value: 'SPORTS'},
                            {key: 5, value: 'CAMPING'},
                            {key: 6, value: 'TRAVELLING'},
                            {key: 7, value: 'ENTERTAINMENT'}
                        ],
                        value: 7,
                        required: true,
                        order: 1
                    }),

                    new TextboxField({
                        key: 'title',
                        placeholder: 'Title',
                        required: true,
                        value: '',
                        order: 2
                    }),
                    new TextboxField({
                        key: 'message',
                        placeholder: 'Message',
                        value: '',
                        required: false,
                        order: 3
                    }),
                    new TextboxField({
                        key: 'amount',
                        placeholder: 'Amount in stock',
                        type: 'number',
                        value: '',
                        required: true,
                        order: 4
                    }),
                    new TextboxField({
                        key: 'price',
                        placeholder: 'Price per coupon',
                        type: 'number',
                        value: '',
                        required: true,
                        order: 5
                    }),
                    new TextboxField({
                        key: 'startDate',
                        placeholder: 'Active since',
                        type: 'date',
                        value: '',
                        required: true,
                        order: 6
                    }),
                    new TextboxField({
                        key: 'endDate',
                        placeholder: 'Ends at',
                        type: 'date',
                        value: '',
                        required: true,
                        order: 7
                    }),
                    new TextboxField({
                        key: 'image',
                        placeholder: 'Choose image for your promotion',
                        type: 'textbox',
                        value: '',
                        required: true,
                        order: 8
                    })
                ];
                break;

            case "Coupon-update":
                fields = [
                    new DropdownField({
                        key: 'type',
                        label: 'Category',
                        options: [
                            {key: 0, value: 'RESTAURANTS'},
                            {key: 1, value: 'ELECTRICITY'},
                            {key: 2, value: 'FOOD'},
                            {key: 3, value: 'HEALTH'},
                            {key: 4, value: 'SPORTS'},
                            {key: 5, value: 'CAMPING'},
                            {key: 6, value: 'TRAVELLING'},
                            {key: 7, value: 'ENTERTAINMENT'}
                        ],
                        value: objectModel.type,
                        required: true,
                        order: 1
                    }),
                    new TextboxField({
                        key: 'message',
                        label: 'Message',
                        value: objectModel.message,
                        required: false,
                        order: 2
                    }),
                    new TextboxField({
                        key: 'amount',
                        label: 'Amount in stock',
                        type: 'number',
                        step: "1",
                        value: objectModel.amount,
                        required: true,
                        order: 3
                    }),
                    new TextboxField({
                        key: 'price',
                        label: 'Price per coupon',
                        type: 'number',
                        step: "0.01",
                        value: objectModel.price,
                        required: true,
                        order: 4
                    }),
                    new TextboxField({
                        key: 'startDate',
                        label: 'Active since',
                        type: 'date',
                        value: objectModel.startDate,
                        required: true,
                        order: 5
                    }),
                    new TextboxField({
                        key: 'endDate',
                        label: 'Ends at',
                        type: 'date',
                        value: objectModel.endDate,
                        required: true,
                        order: 6
                    }),
                    new TextboxField({
                        key: 'image',
                        label: 'Choose image for your promotion',
                        type: 'textbox',
                        value: objectModel.image,
                        required: true,
                        order: 7
                    })
                ];
                break;
        }

        //this._logger.setLog(" fields = " + JSON.stringify(fields));

        return fields === undefined ? fields : fields.sort((a, b) => a.order - b.order);
    }
}
