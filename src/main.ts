import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
//import { platformBrowser } from '@angular/platform-browser';
import { enableProdMode } from '@angular/core';
import { AppModule } from './app/app.module';
import "./material2-app-theme.scss"
//import { AppModuleNgFactory } from '../aot/app/app.module.ngfactory';
if (process.env.ENV === 'production') {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule);
//platformBrowser().bootstrapModule(AppModule);

//AOT compiler
//import { platformBrowser }    from '@angular/platform-browser';
//platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
//JIT compiler
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { AppModule }              from './app.module';
// platformBrowserDynamic().bootstrapModule(AppModule);
