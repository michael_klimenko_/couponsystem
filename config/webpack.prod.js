var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');


const app = "CouponWeb";                         // URL application is deployed at
const rest = "rest";                               // URL to web api
const server = "http://37.26.145.46";           // URL to the web server
const port = "8080";

const login = "login";
const coupons = "coupons";
const companies = "companies";
const customers = "customers";

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';


module.exports = webpackMerge(commonConfig, {
   devtool: 'source-map',

    output: {
        path: helpers.root('dist'),
        publicPath: '/CouponWeb/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    htmlLoader: {
        minimize: false // workaround for ng2
    },

    plugins: [
        //new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin('[name].[hash].css'),
        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify(ENV),
                //login URL
                'loginUrl': JSON.stringify(rest+'/'+login),
                //'loginUrl': JSON.stringify(server+':'+port+'/'+app+'/'+rest+'/'+login),
                //services URLs
                'couponsUrl': JSON.stringify(rest+'/'+coupons),
                'companiesUrl': JSON.stringify(rest+'/'+companies),
                'customersUrl': JSON.stringify(rest+'/'+customers)
                //'couponsUrl': JSON.stringify(server+':'+port+'/'+app+'/'+rest+'/'+coupons),
                //'companiesUrl': JSON.stringify(server+':'+port+'/'+app+'/'+rest+'/'+companies),
                //'customersUrl': JSON.stringify(server+':'+port+'/'+app+'/'+rest+'/'+customers)
            }
        }),
        new CopyWebpackPlugin([

            { from: helpers.root('public'),
                to: helpers.root('dist/assets')
            },
        ], {
            copyUnmodified: true
        })
    ]
});
