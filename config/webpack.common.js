var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');


module.exports = {
    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': ['./src/main.ts']
    },

    resolve: {
        root: __dirname,
        alias: {
            //materializecss: 'materialize-css/dist/css/materialize.css',
            //materialize: 'materialize-css/dist/js/materialize.js',
            //jquery: 'jquery/dist/jquery',
            //jquery: "jquery/src/jquery",
            images: 'public/images/'
        },
        modulesDirectories: ["node_modules"],
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js', '.css']
        //extensions: ['', '.js', '.ts']
    },
    resolveLoader: {
        modulesDirectories: ["node_modules"],
        moduleTemplates: ["*-loader", "*"],
        extensions: ["", ".js"],
    },

    module: {
        noParse: [/.+zone\.js\/dist\/.+/, /.+angular2\/bundles\/.+/],
        // noParse: [
        //    // "config"
        //   wrapRegexp(/^((?!config).)*\.js$/,' noParse'),
        //   helpers.root('config'),
        // ],
        loaders: [
            // {
            //   test: /\.js$/,
            //   exclude: /^((?!\.config\.).)*\.js$/,
            //   loader: 'babel'
            // },

            {
                test: /\.ts$/,
                loaders: ['ts', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.(woff|woff2)$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff&name=assets/fonts/[name].[ext]"
            },
            {test: /\.ttf$/, loader: "url-loader?limit=10000&name=assets/fonts/[name].[ext]"},
            {test: /\.eot$/, loader: "url-loader?limit=10000&name=assets/fonts/[name].[ext]"},
            {test: /\.svg$/, loader: "url-loader?limit=10000&name=assets/images/[name].[ext]"},
            // { test: /\.png$/,    loader: "url-loader?limit=10000&name=assets/images/[name].[ext]" },
            //{ test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/, loader: 'url-loader?limit=100000' },
            {
                test: /\.(png|jpe?g|gif|ico)$/,
                loader: 'file?name=assets/images/[name].[ext]'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract("style", "css!sass")
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style", "css!less")
            },
            {
                test: /\.css$/,
                //test: /^((?!materialize).)*\.css$/,
                exclude: helpers.root('src', 'app'),
                loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
            },
            // {
            //     test: /materialize\.css$/,
            //     loader: 'style!css'
            // },
            // {
            //     //test: wrapRegexp(/materialize-css\/dist\/js\/materialize\.js/,'materialize CSS test :'),
            //     test: /materialize\.js/,
            //     loader: 'imports?materializecss'
            // },
            {
                test: /\.css$/,
                //avoid materialize.css
                //test: /^((?!materialize).)*\.css$/,
                include: helpers.root('src', 'app'),
                loader: 'raw'
                //loaders:[ExtractTextPlugin.extract('style/url!file?name=css/[name].[ext]', 'css'), 'to-string', 'css']
                //loaders:[ExtractTextPlugin.extract('style', 'css'), 'to-string', 'css']
                //loader: ExtractTextPlugin.extract('style/url!file?name=css/[name].[ext]')
                //loader: 'style/url!file?name=css/[name].[ext]!css'

            },
        ]
    },

    plugins: [
        // new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jQuery: "jquery",
        //     "window.jQuery": "jquery",
        //     Hammer: "hammerjs/hammer"
        // }),
        // new webpack.ProvidePlugin({
        //     'window.jQuery': 'jquery',
        //     'window.$': 'jquery',
        //     'jQuery': 'jquery',
        //     $: 'jquery',
        //     Hammer: "hammerjs/hammer"
        // }),

        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),

        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
    ],
    // externals: {
    //   jquery: 'jQuery'
    // }
};

//wrapper for troubleshooting Regex - adding console to method test()
function wrapRegexp(regexp, label) {
    regexp.test = function (path) {
        console.log(label, path);
        return RegExp.prototype.test.call(this, path);
    };
    return regexp;
}
