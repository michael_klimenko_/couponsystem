//not in use
module.exports = {
    styleLoader: require('extract-text-webpack-plugin').extract('style', 'css!less'),

    module: {
        loaders: [
            // the url-loader uses DataUrls.
            // the file-loader emits files.
            { test: /\.(woff|woff2)$/,  loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf$/,    loader: "file-loader" },
            { test: /\.eot$/,    loader: "file-loader" },
            { test: /\.svg$/,    loader: "file-loader" }
        ]
    },
    scripts: {
        // add every bootstrap script you need
        'transition': true
    },
    styles: {
        // add every bootstrap style you need
        "mixins": true,

        "normalize": true,
        "print": true,

        "scaffolding": true,
        "type": true,
    }
};