var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const app = "CouponWeb";                         // URL application is deployed at
const rest = "rest";                               // URL to web api
//const server = "http://10.192.210.100";           // URL to the web server
const server = "http://localhost";
const port = "8080";

const login = "login";
const coupons = "coupons";
const companies = "companies";
const customers = "customers";

module.exports = webpackMerge(commonConfig, {
    //devtool: 'cheap-module-eval-source-map',
    devtool: 'eval-source-map',
    output: {
        path: helpers.root('dist'),
        publicPath: '/CouponWeb/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('css/[name].css'),
        new webpack.DefinePlugin({
            'process.env': {
                //login URL
                // 'loginUrl': JSON.stringify(rest+'/'+login),
                'loginUrl': JSON.stringify(server + ':' + port + '/' + app + '/' + rest + '/' + login),
                //services URLs

                // 'couponsUrl': JSON.stringify(rest+'/'+coupons),
                // 'companiesUrl': JSON.stringify(rest+'/'+companies),
                // 'customersUrl': JSON.stringify(rest+'/'+customers)
                'couponsUrl': JSON.stringify(server + ':' + port + '/' + app + '/' + rest + '/' + coupons),
                'companiesUrl': JSON.stringify(server + ':' + port + '/' + app + '/' + rest + '/' + companies),
                'customersUrl': JSON.stringify(server + ':' + port + '/' + app + '/' + rest + '/' + customers)
            }
        }),
        //CopyWebpackPlugin has a bug  - it is copying empty files during building
        // using for copying assets/images
        new CopyWebpackPlugin([
            // Copy directory contents to {output}/to/directory/
            {
                from: helpers.root('public'),
                to: helpers.root('dist/assets')
                //to: '/Users/michael/Documents/NetBeansProjects/CouponREST/target/CouponREST-1.0-SNAPSHOT'
            },
            // {
            //   from: helpers.root('dist') + 'css/app.css',
            //   to: '/Users/michael/Documents/NetBeansProjects/CouponREST/target/CouponREST-1.0-SNAPSHOT/css'
            // }
        ], {
            // By default, we only copy modified files during
            // a watch or webpack-dev-server build. Setting this
            // to `true` copies all files.
            copyUnmodified: true
        })
    ],
    stats: {
        colors: true,
        errorDetails: true,
        modules: false,
        reasons: true   // add information about the reasons why modules are included
    },

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
        // This is required for webpack-dev-server if using a version <3.0.0.
        // The path should be an absolute path to your build destination.
        //outputPath: path.join(__dirname, 'build')
    }
});
