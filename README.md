# Coupons : Phase II
## This repository includes Web Client for RESTfull Services described in [Phase II](https://bitbucket.org/michael_klimenko_/couponrest) ###

#### What is this repository for? ####

Coupon management system is intended to be a proof of concept for themes learned at Java Course.
It is the last part of Coupon project, and it introduces "User Interface" 

The following parts are required for tests:

* Backend [Coupon Project Phase I](https://bitbucket.org/michael_klimenko_/coupons)
* REST    [Coupon Project Phase II](https://bitbucket.org/michael_klimenko_/couponrest)


===================


### How do I get set up? ###



# Dependencies

* Development
-[Node.js](https://nodejs.org)

* Build 
-[Webpack module bundler](https://webpack.github.io/docs/what-is-webpack.html)

* Framework 
-[Angular 2 RC5](https://angular.io/)

* Security 
-[Json Web Token](https://jwt.io/)

* CSS 
-[Angular2 Material](https://material.angular.io/)


# Configuration

1. clone this repo to your workspace.
2. run `npm install` to get all the required packages

# Database configuration
run `Tester.java` from Backend Phase I to initialise DB with mockup values

# Deployment instructions
1. run `npm run build`
2. copy content of `dist` folder to web server running Phase I & Phase II.


# Known Issues
* localStorage "id_token" is not removed at sign out.
* Coupon's type category is returning string value instead of int at update coupon details.
* Mobile version looks bad with sidenav.
* CSS in dark mode needs some tweaking.


# TODOs
* add search box in customer dashboard.
* implement more sofisticated filters for coupons in customer dashboard.
* add toast notification pop-up when error/warning/success.
* add user profiles in customer site version to be able to change CSS scheme.
* implement pagination for all views.
* implement and show coupon's company get by coupon , gett all customers that bought specific coupon.
* implement image uploader.
* add button to populate database with some mockup values.
* write some basics tests.

### Who do I talk to? ###

* michael.klimenko@gmail.com


